# ROBO FRIEND

# [Demo](https://mahbuburtuhin.gitlab.io/robofriend/)

## Description 

This React application is a single page application (SPA) which fetches data from a third party API and gererates image form a backend server and it helps user to filter robot name provided in search box.

## Screenshot

![UI Screenshot](screenshot.png)

## Available Scripts

In the project directory, you can run:

### npm start

Runs the app in the development mode.
The page will reload if you make edits.
You will also see any lint errors in the console.

### npm test

Launches the test runner in the interactive watch mode.<br />

### npm build

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!
